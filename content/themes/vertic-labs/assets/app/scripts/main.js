/*
 * Vertic JS - Site functional wrapper
 * http://labs.vertic.com
 *
 * Copyright 2012, Vertic A/S
 *
 */

/*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require: true, Modernizr: true*/

/* Remember to update Gruntfile.js with config */
require.config({
	paths: {
		'jquery': '../components/jquery/jquery',
		'velocity': '../components/velocity/velocity',
		'highlight': '../components/highlight.js/src/highlight'
	},
	shim: {
		'highlight': {
			deps: ['jquery']
		}
	}
});

require([
	'framework/core', 
	'jquery', 
	'velocity',
	'highlight'
	], function(
		core, 
		$, 
		velocity,
		highlight 
	){
	'use strict';

	// Expose core as vertic first for debugging reasons
	window.vertic = core;

	console.time('loadtime');
	// Document ready
	$(function () {

		hljs.initHighlightingOnLoad();
		openContent();

		function openContent() {
			var mainSlideLength = $('.main-slide').length,
				$all = $('.main-slide'),
				$activeSlide,
				activeSlide = 'active-slide',
				slideWidth = 100/mainSlideLength+'%',
				activeSlideWidth = 80,
				inactiveSlideWidth = (100 - activeSlideWidth)/(mainSlideLength-1),
				thisDuration = 500,
				DIRECTION ={LEFT:'left', RIGHT:'right'},
				isAnimating = false;
				
			$all.css({width:slideWidth});

			function blackBurgerMenu(){

				if($all.last().hasClass('active-slide')){
					$('.btn-burger').addClass('btn-burger-black');
				} else{
					$('.btn-burger').removeClass('btn-burger-black');
				}
			}

			function collapseAll() {
			
				//collapsing slides
				$all.velocity("stop").velocity({
					width:slideWidth
				}, {
					duration:thisDuration,
					complete: function() {
						//$('body').off('activeSlide:open');
						$all.off('mouseenter mouseleave').removeClass(activeSlide);
					}
				});
			
				$('.btn-burger').removeClass('btn-burger-black');	
				
				$all.find('.post').velocity({
					translateX: 0
				}, {
					duration:thisDuration
				});

				//collapsing white area in the posts
				$all.find('.post-content').addClass('post-content-active').velocity("stop").velocity({
					opacity:0,
					translateX: '-50px'	
				});	
			};

			$all.on('click',function(){
				//avoiding the post to jump
				if($(this).hasClass('active-slide')) return false;
				var $this = $(this);
				
				$all.removeClass(activeSlide);

				$all.find('.post-content').addClass('post-content-active').velocity("stop").velocity({
					opacity:0,
					translateX:'-50px'
				}, {
					complete:function(){
						
						$this.find('.post-content').removeClass('post-content-active').velocity("stop").velocity({
							opacity:1,
							translateX:'+1px'
						});	
						
						//All slides go back go normal
						$all.velocity({
							translateX: 0
						});
					}
				});	
				
				// Set active class
				$activeSlide = $(this).addClass(activeSlide);

				$activeSlide.velocity("stop").velocity({
					width: activeSlideWidth+"%"}, {
					duration:thisDuration
				}).siblings().velocity("stop").velocity({
					width: inactiveSlideWidth+'%'}, {
					duration:thisDuration
				});

				$('body').trigger('activeSlide:open');

				$(this).find('.post').velocity({
					translateX:0,
					width: "100%"
				}, {
					duration: thisDuration
				});
				$(this).removeClass('slidehovering');

				blackBurgerMenu();
			});

			// Mouseenter over unactive slides
			$('body').on('activeSlide:open', function() {

				console.log('activeSlide:open');

				$all.on('mouseenter',function(){
					var thisIndex = $(this).index();

					console.log('mouseenter');

					// Previous slide
					if($activeSlide.index() > thisIndex && $all.hasClass(activeSlide)){ 
						$(this).find('.post').css({
							width:$('.post-header').outerWidth()+'px'
						});
						$(this).nextAll().velocity('stop').velocity({
							translateX: ( $('.post-header').outerWidth() - $activeSlide.siblings().width() )+'px'
						});
						$(this).velocity('stop').velocity({
							translateX:0
						}).addClass('slidehovering');

					// Next slide
					} else if($activeSlide.index() < thisIndex && $all.hasClass(activeSlide)){

						$(this).find('.post').css({
							width:$('.post-header').outerWidth()+'px'
						})

						$(this).find('.post').velocity('stop').velocity({
							translateX: -( $('.post-header').outerWidth() - $activeSlide.siblings().width() )+'px'
						});
						$(this).addClass('slidehovering');
					};		

				}).mouseleave(function() {
					var $this = $(this),
						thisIndex = $(this).index();

					console.log('mouseleave');
					//Mouseleaving of previous slide	
					if($activeSlide.index() > thisIndex && $all.hasClass(activeSlide)){

						$(this).nextAll().velocity('stop').velocity({
							translateX:0
						}, {
							complete: function() {
								$(this).find('.post').css({width:'100%'});
							}
						});	
					//Mouseleaving of next slide	
					} else if($activeSlide.index() < thisIndex && $all.hasClass(activeSlide)){ 

						$(this).find('.post').not($('.slidehovering')).velocity('stop').velocity({
							translateX:0
						}, {
							complete: function() {
								$(this).find('.post').css({width:'100%'});
							}
						});		
					}

					$all.removeClass('slidehovering').velocity('stop').velocity({
						translateX:0
					});
				});
			});

			function keySliding(direction){
				/*var	next = $activeSlide.index()+2,
					previous = $activeSlide.index(),
					index = $activeSlide.index(),*/
				
				var	chosenIndex;

				if(direction === DIRECTION.LEFT){
					if($all.hasClass(activeSlide) == false){
						
						$activeSlide = $all.last().addClass(activeSlide);
						$('body').trigger('activeSlide:open');
					} else if($activeSlide.index()+1 == 1){
						
						$all.removeClass(activeSlide);
						$activeSlide = $all.last().addClass(activeSlide);
					} else {

						$activeSlide = $activeSlide.prev();
					}
					chosenIndex = $activeSlide.index();

				}

				else if(direction === DIRECTION.RIGHT){
					if($all.hasClass(activeSlide) == false){
						
						$activeSlide = $all.first().addClass(activeSlide);
						$('body').trigger('activeSlide:open');
					} else if($activeSlide.index()+1 == $all.length){
						
						$all.removeClass(activeSlide);
						$activeSlide = $all.first().addClass(activeSlide);
					} else {

						$activeSlide = $activeSlide.next();
					}
					chosenIndex = $activeSlide.index();
					
				}

				console.log('activeSlide', $activeSlide.index());


				$('.main-slide:nth-child('+(chosenIndex+1)+')').addClass(activeSlide).siblings().removeClass(activeSlide);	
				$('.active-slide').velocity("stop").velocity({
					width: activeSlideWidth+"%"}, {
					duration:thisDuration
				}).siblings().velocity("stop").velocity({
					width: inactiveSlideWidth+'%'}, {
					duration:thisDuration
				});

				$all.find('.post-content').addClass('post-content-active').velocity("stop").velocity({
					opacity:0,
					translateX:'-50px'
				}, {
					complete:function(){
						
						$('.active-slide').find('.post-content').removeClass('post-content-active').velocity("stop").velocity({
							opacity:1,
							translateX:'0'
						});	
						
						$all.velocity({
							translateX: 0
						});
					}
				});	

				$('.active-slide').find('.post').velocity({
					translateX:0,
					width: "100%"
				}, {
					duration: thisDuration
				});
			}

			$("body").keydown(function(e) {
				var key = e.keyCode ? e.keyCode : e.which;				

				if(key == 37) { // left		
					keySliding(DIRECTION.LEFT);
				} else if(key == 39) { // right
					keySliding(DIRECTION.RIGHT);
				} else if(key == 40) {
					collapseAll();
				}
				blackBurgerMenu();
			}); 
		};


		//Navigation on right
		$('.nav-close').on('click',function(){
			var animDuration = 500;

			$(this).toggleClass('btn-burger-open');

			if($(this).hasClass('btn-burger-open') ){
				$('.sitewrap').velocity("stop", true).velocity({translateX:"-30%"},{duration:animDuration});
				
				//Adds an overlay
				$('.main-articleswrap').addClass('overlay');
			} else {
				$('.sitewrap').velocity("stop", true).velocity({translateX:"0"},{duration:animDuration});
				
				//Removes overlay
				$('.main-articleswrap').removeClass('overlay');
			}

			//If last slide in row = open --> turn burgermenu into black
			if($('.main-slide').last().hasClass('active-slide') && $(this).hasClass('btn-burger-black')){
				$('.btn-burger').removeClass('btn-burger-black');
			} else if(!$(this).hasClass('btn-burger-open') && $('.main-slide').last().hasClass('active-slide')){
				$('.btn-burger').addClass('btn-burger-black');
			}

			$('.main-articleswrap').on('click', function() {
				$('.nav-close').trigger('click');
			});
		});
	});
		console.timeEnd('loadtime');
		console.log('start loadtime = 0.976 ms');
});
